# Contributing

1. Create user on gitlab.alpinelinux.org
2. Fork this repo
3. Push your changes to your fork
4. Create a merge request
5. wait for review and do adjustments asked by the maintainers

NOTE: it is recommended you use a text editor that shows tabs and spaces, like
vim with the `list` option set.

# Building

1. First install `redo` and `scdoc` from the community repository
2. Create the man pages with `redo build`

# Installing

1. Edit conf to add or remove binaries and manapges to be installed
2. Install with `redo install`, use DESTDIR, PREFIX, MANDIR and BINDIR to change
 default location of where the project is installed

# Testing

1. First install `redo` and `bats` from the community repository
2. Install `lua5.3` from the main repository
3. Make sure the directory containing `./apkbuild-lint` is on `$PATH`
4. Run the tests with `redo check`

# TASKS

This is a small list of things from the author (Leo) that wants to happen but
has neither the time or will to do it at the present moment, this can be
combined with the issues in this repo to see what is in need to be done:

- Make missing-default-prepare [AL54] deal with commands after ;
- Make `secfixes-check` compatible with our nix flake's devshell (it currently has `#!/usr/bin/lua5.3` but nixpkgs only provides `lua` in `$PATH`)

## License

See LICENSE
